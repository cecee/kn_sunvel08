package com.cecee.ky_sunvel;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import needle.Needle;
import needle.UiRelatedTask;

import static com.cecee.ky_sunvel.MainActivity.SCALE_PLATE_NUM;

public class PeriodChangeCheckRunnable implements Runnable {
    private static final String TAG = "#@#";
    private final int WAITmillisecond = 100;
    private final int Testmillisecond = 1000;
    Context mContext;
    private boolean stopped = false;
    public  boolean MEASURE_SCALE = true;
    private int waitMillisec;
    private int loopCnt = 0;
    private int tare_cnt = 0;
    private int lweight=0;
    private JSONObject keep_takeOutObj;
    PeriodChangeCheckRunnable(Context context) {
        mContext = context;
    }
    int[] exScale_data=new int[16];
    int[] mScale;
    int[][] ex_Scale_view=new int[3][16];
    //private ArrayList<ArrayList<Integer>> pushback_Array_List;
    private int get_measure_cnt=0;
    @Override
    public void run() {
        while (!stopped) {
           // Log.d(TAG,"#@# (1)======== loop ========TEST["+((MainActivity) mContext).FACTORY_TEST +"]");
            loopCnt++;
            waitMillisec = (((MainActivity) mContext).FACTORY_TEST) ? Testmillisecond: WAITmillisecond;
            //date view update
            if(loopCnt>30){
                loopCnt=0;
                ((MainActivity) mContext).currentStatus.fun_CurrentDateTimeFromSystemTime();
                ((MainActivity) mContext).currentStatus.funSetCurrent_date_timeViewUI();
            }
          //  ((MainActivity) mContext).jni.jniSetLed(((MainActivity) mContext).currentStatus.getItemViewColor());

            if(MEASURE_SCALE)   mScale = ((MainActivity) mContext).jni.jniGetScale();
            //else mScale = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            // Log.d(TAG,"#@# (2)======== loop ========TEST["+((MainActivity) mContext).TEST+"]");
//시간절약을 위하여 잘못되었다면 다시 while문 으로
//            if(mScale[16]>0 || (mScale[0]==0 && mScale[1]==0)) {
//                try {
//                    Thread.sleep(200);
//                } catch (Exception e) {e.printStackTrace();}
//                 continue;
//            }//error이면
//                try {
//                    Thread.sleep(1);
//                } catch (Exception e) {e.printStackTrace();}

            int[] mDataset = scale_raw2view(mScale);
            changing_process(mDataset);
            ((MainActivity) mContext).currentStatus.setScale_raw(mScale);
            ((MainActivity) mContext).currentStatus.setScale_data(mDataset);
            ((MainActivity) mContext).currentStatus.fun_CurrentDateTimeFromSystemTime();

            if(((MainActivity) mContext).FACTORY_TEST){
                int[] led_color=new int[16];
                ((MainActivity) mContext).loop_cnt %=5;
                Arrays.fill(led_color, ((MainActivity) mContext).loop_cnt++);
                ((MainActivity) mContext).jni.jniSetLed(led_color);
                Log.d(TAG, "#@# ======== LED_TEST !!! ========"+led_color);
            }
            else ((MainActivity) mContext).jni.jniSetLed(((MainActivity) mContext).currentStatus.getItemViewColor());

            try {
                Thread.sleep(waitMillisec);
            } catch (Exception e) {e.printStackTrace();
            }
            exScale_data=mDataset;
            //Log.d(TAG, "#@# PeriodChangeCheckRunnable~~~");
        }
    }

    public boolean change_detection(int[] current, int[] before) {
        boolean changed=false;
        for(int i=0;i<SCALE_PLATE_NUM;i++){
            if(Math.abs(current[i]-before[i])>2){
                changed=true;
                break;
            }
        }
        return changed;
    }

    public void changing_process(int[] mDataset) {
        Needle.onBackgroundThread().execute(new UiRelatedTask<JSONObject>() {
            @Override
            protected JSONObject doWork() {
                JSONObject jobj=((MainActivity) mContext).currentStatus.make_combination_object(mDataset);
                return jobj;
            }
            @Override
            protected void thenDoUiRelatedWork(JSONObject jobj) {
                Log.d(TAG,"#@# @@@@@@@@@@(AAA) jobj ->"+ jobj.toString());
                if (((MainActivity) mContext).menu_position == 0) {
                    if (!((MainActivity) mContext).FACTORY_TEST) {
                        make_combination_view(jobj,mDataset);
                    }
                    //색상결정
                    //Value 결정
                    // if(!jobj.isNull("take_in"))
                    if(((MainActivity) mContext).currentStatus.TAKE_OUT_READY && mDataset[0]<50){
                        ((MainActivity) mContext).currentStatus.TAKE_OUT_READY=false;
                        ((MainActivity) mContext).currentStatus.system_total_box_num++;//BOX CNT INC
                        ((MainActivity) mContext).currentStatus.system_total_weight=((MainActivity) mContext).currentStatus.system_total_weight+lweight;

                        Log.d(TAG,"#@# @@@@@@@@@@(AAA) system_total_weight ->"+ ((MainActivity) mContext).currentStatus.system_total_weight+"  lweight:"+lweight);
                        String str= ((MainActivity) mContext).Formatter_3digit.format(((MainActivity) mContext).currentStatus.system_total_box_num);
                        str = String.format("%s 박스", str);
                        ((MainActivity) mContext).button_box_clear.setText(str);
                        if(keep_takeOutObj !=null) { //ppp 221125
                            //Log.d(TAG, "#@# dbUpdate_today_product keep_takeOutObj->" + keep_takeOutObj.toString());
                            ((MainActivity) mContext).db_manager.dbUpdate_today_product(keep_takeOutObj);//db update
                        }
                    }
                    String str_box_weight= ((MainActivity) mContext).Formatter_3digit.format(((MainActivity) mContext).currentStatus.system_box_weight);
                    ((MainActivity) mContext).textView_system_box_weight.setText(str_box_weight);
                    ((MainActivity) mContext).recyclerManager.mAdapter_scale.notifyDataSetChanged();
                }
            }
        });
    }


    public void make_combination_view(JSONObject result, int[] value) {
        Log.d(TAG,"#@# make_combination_view take out result["+ result.length()+"] ->"+result.toString());
        JSONObject takeOutObj;
        if(result.length()<1) return;
        try {
            takeOutObj = result.getJSONObject("take_out");
            set_takeOut_textView(takeOutObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //((MainActivity) mContext).currentStatus.setEx_jArry_combi(takeInJarry);
    }
///ppp
    void set_takeOut_textView (JSONObject jobj) {
        int jarrCnt=jobj.length();
        Log.d(TAG, "#@# take out set_complete_textView()->" + jobj.toString()+"|"+jarrCnt);
        if(0==jarrCnt) {
            return;
        }
        // for (int i = 0; i < jarrCnt; i++) {
        try {
            //Log.d(TAG, "#@# @@@@@@@ set_takeOut_textView mjobj.toString()->" + mjobj.toString());
            keep_takeOutObj=jobj;
            lweight=jobj.getInt("weight");
            //((MainActivity) mContext).currentStatus.system_total_box_num++;//BOX CNT INC
            //((MainActivity) mContext).currentStatus.system_total_weight=((MainActivity) mContext).currentStatus.system_total_weight+lweight;
            //String str= ((MainActivity) mContext).Formatter_3digit.format(((MainActivity) mContext).currentStatus.system_total_box_num);
            //str = String.format("%s 박스", str);
            //((MainActivity) mContext).textView_system_box_weight.setText(str_box_weight);
            //((MainActivity) mContext).button_box_clear.setText(str);
            //((MainActivity) mContext).db_manager.dbUpdate_today_product(jobj);//db update
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //  }

    }

    void set_combination_textView(JSONArray jarr) {
        int jarrCnt=jarr.length();
        //int[] check={0,0,0,0};
        int[] items_color=new int[14];
        int[] items_weights=new int[4];
        Arrays.fill(items_color, 0);
        Arrays.fill(items_weights, 0);

        int[] Occupy_color =((MainActivity) mContext).currentStatus.getOccupy_color();//[tmp] = 0x00;
        Arrays.fill(Occupy_color, 0);
        for (int i = 0; i < jarrCnt; i++) {
            try {
                JSONObject mjobj = jarr.getJSONObject(i);
                JSONArray mjarrIdx = mjobj.getJSONArray("idx");
                JSONArray mjarrVal = mjobj.getJSONArray("value");
                int weight= mjobj.getInt("weight");
                int combi_idx= mjobj.getInt("combi_idx");
                Occupy_color[combi_idx]=0xAA;
                if(combi_idx<4) items_weights[combi_idx]=weight;
                //check[combi_idx]=1;
                //viewChangeIdx(combi_idx,weight);
                int combi_cnt = mjarrIdx.length();
                for (int ix = 0; ix < combi_cnt; ix++) {
                    int _idx = mjarrIdx.getInt(ix);
                    items_color[_idx] = combi_idx+1;
                }
                Log.d(TAG, "#@# @@@@@@@@@@ mjobj.toString()->" + mjobj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ((MainActivity) mContext).currentStatus.setOccupy_color(Occupy_color);
        viewChangeIdx(items_weights);
        // ((MainActivity) mContext).currentStatus.setItemViewColor(items_color);
    }

    public void viewChangeIdx (int[] weights) {
        for(int i=0; i<4;i++) {
            String formattedStringNum = ((MainActivity) mContext).Formatter_3digit.format(weights[i]);
            String str = String.format("%s g", formattedStringNum);
            switch (i) {
                case 0:
                    ((MainActivity) mContext).textView_combi_sum1.setText(str);
                    break;
                case 1:
                    ((MainActivity) mContext).textView_combi_sum2.setText(str);
                    break;
                case 2:
                    ((MainActivity) mContext).textView_combi_sum3.setText(str);
                    break;
                case 3:
                    ((MainActivity) mContext).textView_combi_sum4.setText(str);
                    break;
            }
        }
    }


    int[] scale_raw2view(int[] raw) {

        int[] m_vraw = new int[SCALE_PLATE_NUM];
        int[] result = new int[SCALE_PLATE_NUM];
        int[] m_tare = ((MainActivity) mContext).currentStatus.getLocal_tare();
        float[] m_factor = ((MainActivity) mContext).currentStatus.getLocal_f1k();
        //get_measure_cnt++;
        //if(get_measure_cnt>3)get_measure_cnt=0;

        for (int i = 0; i < SCALE_PLATE_NUM; i++) {
            float factor = m_factor[i];
            int real_scale = (int) ((raw[i] * factor) - (m_tare[i] * factor));
            //ppp 2022.09.08
            //real_scale=(real_scale<5) ? 0:real_scale;//scale view값이 마이너스로 나오면 0로
 //영정이 틀어진거 다시영점잡고 재계산+++
            ex_Scale_view[0][i]=ex_Scale_view[1][i];
            ex_Scale_view[1][i]=ex_Scale_view[2][i];
            ex_Scale_view[2][i]=real_scale;
            //comp??
            //Log.d(TAG,"EX@RAW[0]    "+ ex_Scale_view[0][i]);
            //Log.d(TAG,"EX@RAW[1]   "+ ex_Scale_view[1][i]);
            //Log.d(TAG,"EX@RAW[2]    "+ ex_Scale_view[2][i]);

           // if(ex_Scale_view[0][i]!=0 && ex_Scale_view[1][i]<3 && ex_Scale_view[2][i]<3)
            if(ex_Scale_view[0][i]<3 && ex_Scale_view[1][i]<3 && ex_Scale_view[2][i]<3){
                //Log.d(TAG,"EX@RAW[SET ZERO adj ["+i+"]   raw->"+ raw[i]+" real_scale->"+real_scale);
                Log.d(TAG,"[1]EX@RAW[0]    "+ ex_Scale_view[0][i] + " "+ex_Scale_view[1][i]+" "+ex_Scale_view[2][i]);
                if(!((MainActivity) mContext).FACTORY_TEST) {
                    Log.d(TAG,"[2]EX@RAW[SET ZERO adj ["+i+"]   raw->"+ raw[i]+" real_scale->"+real_scale);
                    ((MainActivity) mContext).currentStatus.setLocal_tare_id(i, raw[i]);
                }
            }
            if(real_scale<3)real_scale=0;


 //영정이 틀어진거 다시영점잡고 재계산---
            m_vraw[i]=real_scale;
        }

        if(((MainActivity) mContext).FACTORY_TEST){
            result[0]=raw[0]+raw[1];
            for (int i = 1; i < SCALE_PLATE_NUM-1; i++) result[i] =raw[i+1];
        }
        else {
            result[0]=m_vraw[0]+m_vraw[1];
            for (int i = 1; i < SCALE_PLATE_NUM-1; i++) result[i] =m_vraw[i+1];
        }
        Log.d(TAG,"X@RAW    "+ Arrays.toString(raw));
        Log.d(TAG,"X@TARE   "+ Arrays.toString(m_tare));
        Log.d(TAG,"X@@FACTOR "+ Arrays.toString(m_factor));
        Log.d(TAG,"X@@View(g)"+ Arrays.toString(result));
        return result;
    }

}
