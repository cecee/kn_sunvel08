package com.cecee.ky_sunvel.mp3;

public interface OnPlayerEventListener {
    void onPlayerSongComplete();
    void onPlayerSongStart(String Title, int songDuration, int songPosition);
}
