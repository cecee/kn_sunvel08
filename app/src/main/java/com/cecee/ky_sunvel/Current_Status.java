package com.cecee.ky_sunvel;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import needle.Needle;

import static com.cecee.ky_sunvel.MainActivity.SCALE_PLATE_NUM;

public class Current_Status implements Serializable {
    protected final String TAG = getClass().getSimpleName();
    Context mContext;

    private JSONArray product_jArry;
    private JSONArray ex_jArry_combi;
    public boolean COMPLETE_COMBINATION = false;
    public boolean exCOMPLETE_COMBINATION = false;
    public boolean TAKE_OUT_READY = false;
    // public JSONObject current_container_obj;

    public int system_total_weight, system_total_box_num, system_box_weight;
    public String current_date_time, current_date, current_time;
    private int[] scale_raw, scale_data, ex_scale_data;
    private int[] itemViewColor, ex_itemViewColor;
    private int[] local_tare, local_i1k, local_i3k, local_i5k;
    private float[] local_f1k, local_f3k, local_f5k;
    private int combi_idx = 0;
    //public int combi_cnt = 0;
    private int[] occupy_color;
    private int[] current_parameter;
    ArrayList<ArrayList<Integer>> gCobination_Array_List;


    public ArrayList<ArrayList<Integer>> getgCobination_Array_List() {
        return gCobination_Array_List;
    }

    public void setgCobination_Array_List(ArrayList<ArrayList<Integer>> gCobination_Array_List) {
        this.gCobination_Array_List = gCobination_Array_List;
    }

    public int[] getOccupy_color() {
        return occupy_color;
    }
    public void setOccupy_color(int[] occupy_color) {
        this.occupy_color = occupy_color;
    }

    Current_Status(Context context) {
        mContext = context;
        this.current_date = "2021-06-01 00:00";
        this.current_date = "2021-06-01";
        this.current_time = "00:00";
        this.scale_raw = new int[16];
        this.scale_data = new int[16];
        this.ex_scale_data = new int[16];
        this.local_tare = new int[16];
        this.local_i1k = new int[16];
        this.local_i3k = new int[16];
        this.local_i5k = new int[16];
        this.local_f1k = new float[16];
        this.local_f3k = new float[16];
        this.local_f5k = new float[16];
        this.ex_jArry_combi = new JSONArray();
        this.itemViewColor = new int[16];
        this.ex_itemViewColor = new int[16];
        this.occupy_color = new int[4];
        this.system_total_weight = 0;
        this.system_total_box_num = 0;
        this.current_parameter = new int[16];
    }

    public int[] getCurrent_parameter() {
        return current_parameter;
    }

    public void setCurrent_parameter(int[] current_parameter) {
        this.current_parameter = current_parameter;
    }

    public JSONArray getEx_jArry_combi() {
        return ex_jArry_combi;
    }

    public void setEx_jArry_combi(JSONArray ex_jArry_combi) {
        this.ex_jArry_combi = ex_jArry_combi;
    }

    public int[] getEx_scale_data() {
        return ex_scale_data;
    }

    public void setEx_scale_data(int[] ex_scale_data) {
        for (int i = 0; i < ex_scale_data.length; i++) this.ex_scale_data[i] = ex_scale_data[i];
        // this.ex_scale_data = ex_scale_data;
    }

    public int[] getEx_itemViewColor() {
        return ex_itemViewColor;
    }

    public void setEx_itemViewColor(int[] ex_itemViewColor) {
        this.ex_itemViewColor = ex_itemViewColor;
    }

    public int[] getItemViewColor() {
        return itemViewColor;
    }

    public void setItemViewColor(int[] itemViewColor) {
        this.itemViewColor = itemViewColor;
    }

    public float[] getLocal_f1k() {
        return local_f1k;
    }

    public void setLocal_f1k(float[] local_f1k) {
        this.local_f1k = local_f1k;
    }

    public float[] getLocal_f3k() {
        return local_f3k;
    }

    public void setLocal_f3k(float[] local_f3k) {
        this.local_f3k = local_f3k;
    }

    public float[] getLocal_f5k() {
        return local_f5k;
    }

    public void setLocal_f5k(float[] local_f5k) {
        this.local_f5k = local_f5k;
    }

    public JSONArray getProduct_jArry() {
        return product_jArry;
    }

    public void setProduct_jArry(JSONArray product_jArry) {
        this.product_jArry = product_jArry;
    }

    public String getCurrent_date_time() {
        return current_date_time;
    }

    public int[] getLocal_tare() {
        return local_tare;
    }

    public void setLocal_tare(int[] local_tare) {
        this.local_tare = local_tare;
    }
    public void setLocal_tare_id(int local_tare_id, int local_tare_value) {
        this.local_tare[local_tare_id]=local_tare_value;
    }
    public void setLocal_box_tare(int[] local_tare) {
        //this.local_tare = local_tare;
        //for (int i = 0; i < 2; i++)   this.local_tare[i] = local_tare[i];
        this.local_tare[0] = local_tare[0];
        this.local_tare[1] = local_tare[1];
    }
    public int[] getLocal_i1k() {
        return local_i1k;
    }

    public void setLocal_i1k(int[] local_i1k) {
        this.local_i1k = local_i1k;
    }

    public int[] getLocal_i3k() {
        return local_i3k;
    }

    public void setLocal_i3k(int[] local_i3k) {
        this.local_i3k = local_i3k;
    }

    public int[] getLocal_i5k() {
        return local_i5k;
    }

    public void setLocal_i5k(int[] local_i5k) {
        this.local_i5k = local_i5k;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(String current_time) {
        this.current_time = current_time;
    }

    public String getCurrent_date() {
        return current_date;
    }

    public void setCurrent_date(String current_date) {
        this.current_date = current_date;
    }


    public int[] getScale_raw() {
        return scale_raw;
    }

    public void setScale_raw(int[] scale_raw) {
        for (int i = 0; i < 16; i++)
            this.scale_raw[i] = scale_raw[i];
    }

    public int[] getScale_data() {
        return scale_data;
    }

    public void setScale_data(int[] scale_data) {
        for (int i = 0; i < scale_data.length; i++) this.scale_data[i] = scale_data[i];
    }

    public void funSetCurrent_date_timeViewUI() {
        Needle.onMainThread().execute(new Runnable() {
            @Override
            public void run() {
                ((MainActivity) mContext).textView_z00_date.setText(current_date_time);
                ((MainActivity) mContext).textView_z02_date.setText(current_date_time);
//                EditText _editText_box_weight = (EditText) ((MainActivity) mContext).findViewById(R.id.editText_box_weight);
//                String str=_editText_box_weight.getText().toString();
//                Log.d("#@#", "B0_View_DoUI(1)  funSetCurrent_date_timeViewUI editText_box_weight f_from->"+str);
            }
        });
    }

    public void fun_CurrentDateTimeFromSystemTime() {
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf_date_time = new SimpleDateFormat("yyyy-MM-dd a hh:mm");
        SimpleDateFormat sdf_date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf_time = new SimpleDateFormat("a hh:mm");
        current_date_time = sdf_date_time.format(date);
        current_date = sdf_date.format(date);
        current_time = sdf_time.format(date);
    }

    public int getIndexColor(int[] mOccupy) {
        int idx = 100;
        for (int i = 0; i < 4; i++) {
            if (mOccupy[i] == 0x00) {
                idx = i;
                break;
            }
        }
        return idx;
    }

    public void mk_gCobination_Array_List(int n, int max_r) {
        ArrayList<ArrayList<Integer>> bucket_array = new ArrayList<ArrayList<Integer>>();
        ArrayList<ArrayList<Integer>> lbucket_array;
        // ArrayList<Integer> nCr_bucket = Get_nCr_bucket(value, weight_min, weight_max);
        int i, sz;
        int[] n_arr = new int[n];

        for (i = 0; i < n; i++) n_arr[i] = i;

        for (int r = 1; r <= max_r; r++) {
            lbucket_array = combination_bucket_array(n_arr, n, r);
            sz = lbucket_array.size();
            for (i = 0; i < sz; i++) {
                ArrayList<Integer> larrlist;
                larrlist = lbucket_array.get(i);
                int szx = larrlist.size();
                //0번이 포함된것만 사용
                for (int ix = 0; ix < szx; ix++) {
                    if (larrlist.get(ix) == 0) {
                        bucket_array.add(larrlist);
                        break;
                    }
                }
            }
            // Log.d(TAG,"#@#  mk_gCobination_Array_List r->"+r+"]  sz->"+sz);//jsonValues->List로 저장
        }
        //Log.d(TAG,"#@# mk_gCobination_Array_List r->"+bucket_array+"]  sz->"+bucket_array.toString());//jsonValues->List로 저장

//
//        Combination comb = new Combination(n, r);
//        comb.combination(bucket_num, 0, 0, 0);
//        ArrayList<ArrayList<Integer>> result = comb.getResult();
        setgCobination_Array_List(bucket_array);
        //return bucket_array;
    }

    public JSONObject make_combination_object(int[] value) {

        int[] items_color = new int[SCALE_PLATE_NUM];
        JSONObject jContainer = new JSONObject();
        //JSONArray mainNewJarry = new JSONArray();
        //JSONArray takeOutJarry = new JSONArray();
        JSONArray jarry = new JSONArray();
        //JSONObject mjobj;
        //if(TAKE_OUT_READY==true) return jContainer;

        int[] combination_item_value = new int[16];
        int tmp;
        combination_item_value = value.clone();
        int[] param = getCurrent_parameter();

        Log.d(TAG, "#@# ------새로운 조합 만듬------------");
        jarry = combination_mix(combination_item_value, param);//새로운 조합 만듬
        //Log.d(TAG, "#@# ------combination_mix 단한개의 새로운 조합  jarry->" + jarry.toString() + jarry.length());
        // ((MainActivity) mContext).RE_COMBINATION = true;
        system_box_weight=0;
        if (jarry.length() > 0) {
            try {
                JSONObject _obj = jarry.getJSONObject(0);
                //Log.d(TAG, "#@# ------combination_mix getJSONObject _obj->" + _obj.toString() + _obj.length());
                //jContainer.put("take_out", _obj);
                Arrays.fill(items_color, 5);//선택되지 않은 칼라 yellow
                //Log.d(TAG, "#@# (111)combination_mix COMPLETE_COMBINATION: "+COMPLETE_COMBINATION+" exCOMPLETE_COMBINATION: "+exCOMPLETE_COMBINATION);
                //system_box_weight=_obj.getInt("weight");
                //Log.d(TAG, "#@# (222)combination_mix----system_box_weight->" + system_box_weight);
                if (_obj.length() > 0) {
                    //Log.d(TAG, "#@# (1)combination_mix----단한개의 새로운 조합  _obj->" + _obj.toString() + _obj.length());
                    //JSONArray _val_arr = _obj.getJSONArray("value");
                    JSONArray _val_idx = _obj.getJSONArray("idx");
                    system_box_weight=_obj.getInt("weight");
                    int combination_buck_cnt = _val_idx.length();
                    //Log.d(TAG, "#@# (111)combination_mix combination_buck_cnt-> " + combination_buck_cnt + "COMPLETE_COMBINATION: "+COMPLETE_COMBINATION+" exCOMPLETE_COMBINATION: "+exCOMPLETE_COMBINATION);
                    if (combination_buck_cnt == 1) {
                        COMPLETE_COMBINATION = true;
                        TAKE_OUT_READY=true;
                        if (!exCOMPLETE_COMBINATION) {
                            //Log.d(TAG, "#@# (222)combination_mix----------단한개의 새로운 조합  _obj->" + _obj.toString() + _obj.length());
                            //Log.d(TAG, "#@# (222)combination_mix----------단한개의 새로운 조합  _val_idx->" + _val_idx.toString() + _val_idx.length());
                            jContainer.put("take_out", _obj);
                        }

                        //Log.d(TAG, "#@# (222)combination_mix----------단한개의 새로운 조합  system_box_weight->" + system_box_weight);
                        items_color[0] = 1;//ppp 221125color_blue
                    } else {
                        COMPLETE_COMBINATION = false;
                        // ((MainActivity) mContext).RE_COMBINATION = true;
                        for (int i = 0; i < _val_idx.length(); i++) {
                            int selected_bucket = _val_idx.getInt(i);
                            if (selected_bucket == 0) items_color[0] = 5;//color_blue 꺼짐
                            else items_color[selected_bucket + 1] = 1;//color_blue
                           // Log.d(TAG, "#@# ----make_combination_object 새로운 조합 바구니  _val_arr->" + _val_idx.toString() + " ->" + selected_bucket);
                        }
                    }
                    //exCOMPLETE_COMBINATION = COMPLETE_COMBINATION;
                }
                else COMPLETE_COMBINATION = false;

                exCOMPLETE_COMBINATION = COMPLETE_COMBINATION;
                setItemViewColor(items_color);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jContainer;
    }

    public ArrayList<ArrayList<Integer>> combination_bucket_array(int[] n_arr, int n, int r) {
        Combination comb = new Combination(n, r);
        comb.combination(n_arr, 0, 0, 0);
        ArrayList<ArrayList<Integer>> result = comb.getResult();
        return result;
    }

    public boolean is_No_Zero_bucked(ArrayList<Integer> bucket) {
        boolean ok = false;
        int sz = bucket.size();
        for (int ix = 0; ix < sz; ix++) {
            int bucket_num = bucket.get(ix);
            if (bucket_num == 0) {
                ok = true;
                break;
            }
        }
        return ok;
    }

    public boolean is_empty_bucked(ArrayList<Integer> bucket, int[] value, int weight_min, int weight_max) {
        boolean ok = false;

        for (int ix = 0; ix < value.length; ix++) {
            if (value[ix] < weight_min || value[ix] > weight_max) value[ix] = 0;
            else value[ix] = value[ix];
        }
        int sz = bucket.size();
        for (int ix = 0; ix < sz; ix++) {
            int bucket_num = bucket.get(ix);
            if (value[bucket_num] == 0 && bucket_num != 0) {
                ok = true;
                break;
            }
        }
        return ok;
    }

    public ArrayList<Integer> Get_nCr_bucket(int[] value, int weight_min, int weight_max) {
        boolean ok = false;
        ArrayList<Integer> nCr_bucket = new ArrayList<Integer>();
        nCr_bucket.add(0);
        for (int i = 1; i < 7; i++) {
            if (value[i] >= weight_min && value[i] <= weight_max) nCr_bucket.add(i);
        }
        return nCr_bucket;
    }

    public ArrayList<ArrayList<Integer>> combination_bucket_total_array(int max_r, int[] value, int weight_min, int weight_max) {
        //int[] n_arr = new int[n];
        int i, n, sz;
        ArrayList<ArrayList<Integer>> bucket_array = new ArrayList<ArrayList<Integer>>();
        ArrayList<ArrayList<Integer>> lbucket_array;

        ArrayList<Integer> nCr_bucket = Get_nCr_bucket(value, weight_min, weight_max);

        n = nCr_bucket.size();
        int[] n_arr = new int[n];
        for (i = 0; i < n; i++) {
            n_arr[i] = nCr_bucket.get(i);
        }
        //Log.d(TAG, "#@#==> get nCr_bucket  combination_bucket_total_array  n->" + n + "  nCr_bucket->" + nCr_bucket.toString());

        for (int r = 1; r <= max_r; r++) {
            lbucket_array = combination_bucket_array(n_arr, n, r);
            sz = lbucket_array.size();
            for (i = 0; i < sz; i++) {
                ArrayList<Integer> larrlist;
                larrlist = lbucket_array.get(i);
                int szx = larrlist.size();
                //0번이 포함된것만 사용
                for (int ix = 0; ix < szx; ix++) {
                    if (larrlist.get(ix) == 0) {
                        bucket_array.add(larrlist);
                        break;
                    }
                }
            }
            // Log.d(TAG,"#@#  combination_bucket_total_array r->"+r+"]  sz->"+sz);//jsonValues->List로 저장
        }
        Log.d(TAG, "#@#  combination_bucket_total_array  sz->" + bucket_array.toString());//jsonValues->List로 저장
        return bucket_array;
    }


    public List<JSONObject> combination_bucket_select_array(int[] value, int[] param) {
        int sz, sz_x, sz_y, ix, iy;
        int combi_min = param[0];
        int combi_max = param[1];
        int weight_min = param[2];
        int weight_max = param[3];
        int boxweight_min = param[4];
        int boxweight_max = param[5];
        //JSONArray jArrCombination = new JSONArray();
        List<JSONObject> jArrCombination_list = new ArrayList<JSONObject>();
        ArrayList<Integer> bucket_weight_ArrayList;
        ArrayList<ArrayList<Integer>> bucket_select_array = new ArrayList<ArrayList<Integer>>();
        ArrayList<ArrayList<Integer>> bucket = new ArrayList<ArrayList<Integer>>();
        ArrayList<ArrayList<Integer>> lbucket_array;
        //  Log.d(TAG, "#@# [COMB]combination_bucket_select_array START" + bucket.toString()+" boxweight_min["+boxweight_min+"] boxweight_max["+boxweight_max+"]");
        // Log.d(TAG, "#@# [xx] value->" + Arrays.toString(value));
        //개체중량 기준범위밖은 제외++
        for (ix = 1; ix < value.length; ix++) {
            if (value[ix] < weight_min || value[ix] > weight_max) value[ix] = 0;
            else value[ix] = value[ix];
        }
        //개체중량 기준범위밖은 제외--
        // Log.d(TAG, "#@# [xx] value->" + Arrays.toString(value));
        ArrayList<ArrayList<Integer>> all_combination = getgCobination_Array_List();
        //  Log.d(TAG, "#@# [COMB](1)combination_bucket_select_array all_combination->" + all_combination.toString());
        ArrayList<Integer> nCr_bucket = Get_nCr_bucket(value, weight_min, weight_max);
        Log.d(TAG, "#@# [COMB](2)combination_bucket_select_array nCr_bucket->" + nCr_bucket.toString());
        ArrayList<Integer> ignore_buck = new ArrayList<Integer>();//= Get_nCr_bucket(value, weight_min, weight_max);
        sz_y = nCr_bucket.size();
        for (ix = 0; ix < 7; ix++) {
            ignore_buck.add(ix);
        }
        for (ix = 0; ix < sz_y; ix++) {
            ignore_buck.remove(Integer.valueOf(nCr_bucket.get(ix)));
        }

        Log.d(TAG, "#@# [COMB](3)combination_bucket_select_array ignore_buck->" + ignore_buck.toString());

        sz = all_combination.size();
        sz_y = ignore_buck.size();

        for (int i = 0; i < sz; i++) {
            ArrayList<Integer> larry = all_combination.get(i);
            boolean ignore = false;
            for (iy = 0; iy < sz_y; iy++) {
                ignore = larry.contains(ignore_buck.get(iy));
                if (ignore) break;
            }
            if (!ignore) bucket.add(larry);
            // Log.d(TAG, "#@# [COMB](3)combination_bucket_select_array larry->" + larry.toString()+" IGNORE->"+ignore);
        }
        Log.d(TAG, "#@# [COMB](4)combination_bucket_select_array bucket->" + bucket.toString());
        try {
            boolean ok = true;
            sz = bucket.size();
            if (sz > 0) {
                for (int i = 0; i < sz; i++) {
                    JSONObject jobj = new JSONObject();
                    JSONArray idx = new JSONArray();
                    JSONArray jsum_weight_value = new JSONArray();
                    bucket_weight_ArrayList = bucket.get(i);
                    sz_x = bucket_weight_ArrayList.size();
                    int sum = 0;
                    //Log.d(TAG,"#@#  combination_bucket_select_array i["+i+"]  sum["+sum +"]->"+bucket_weight_ArrayList.toString());//jsonValues->List로 저장
                    for (ix = 0; ix < sz_x; ix++) {
                        int bucket_num = bucket_weight_ArrayList.get(ix);
                        int bucket_weight = value[bucket_num];
                        // Log.d(TAG,"#@#  bucket_num ["+bucket_num+"]  bucket_weight["+bucket_weight+"]" );//jsonValues->List로 저장
                        sum = sum + bucket_weight;
                        idx.put(bucket_num);
                        jsum_weight_value.put(bucket_weight);
                    }

                    int shift = (sum - boxweight_min);//마이너스오차는 제외시킴
                   //  Log.d(TAG, "#@# [xx] sum[" + sum+"] bucket_weight_ArrayList->"+bucket_weight_ArrayList.toString());
                    //Log.d(TAG, "#@# [XXQ] sum->" + sum+" boxweight_min->"+boxweight_min+" boxweight_max->"+boxweight_max);
                    if ((sum >= boxweight_min) && (sum <= boxweight_max) && (shift >= 0) && sum>0) {
                        jobj.put("weight", sum);
                        jobj.put("shift", shift);
                        jobj.put("idx", idx);
                        jobj.put("value", jsum_weight_value);
                        jArrCombination_list.add(jobj);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Log.d(TAG, "#@# (1)combination_bucket_select_array jArrCombination->" + jArrCombination_list.toString());//jsonValues->List로 저장
        return jArrCombination_list;
    }

    public JSONObject combination_bucket_sort_array(List<JSONObject> jArrCombination_list) {
        //JSONArray sortedJsonArray = new JSONArray();
        Collections.sort(jArrCombination_list, new Comparator<JSONObject>() {
            private static final String KEY_NAME = "shift";

            @Override
            public int compare(JSONObject a, JSONObject b) {
                int valA = 0;// = new String();
                int valB = 0;//String valB = new String();
                try {
                    valA = a.getInt(KEY_NAME);
                    valB = b.getInt(KEY_NAME);
                } catch (JSONException e) {
                }
                int val = (valA - valB) * (1);
                return val;
            }
        });
        //제일 첫번째만 남김++
        JSONObject jsonResult = new JSONObject();
        if (jArrCombination_list.size() > 0) jsonResult = jArrCombination_list.get(0);
        //제일 첫번째만 남김--
//        try {
//            Thread.sleep(1);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Log.d(TAG, "#@#  (2)combination_bucket_sort_array jsonResult->" + jsonResult.toString());//jsonValues->List로 저장
        return jsonResult;
    }

    public JSONArray combination_mix(int[] value, int[] param) {
        int[] combi_value = new int[value.length];//deep copy해서 사용
        int combi_min = param[0];
        int combi_max = param[1];
        int weight_min = param[2];
        int weight_max = param[3];
        int boxweight_min = param[4];
        int boxweight_max = param[5];
        JSONArray jArrCombination = new JSONArray();
        ArrayList<ArrayList<Integer>> lbucket_array;
        List<JSONObject> jArrCombination_list;
        // lbucket_array = combination_bucket_total_array(combi_max+1, value, weight_min, weight_max);
        // Log.d(TAG, "#@# (1)@@@@@@@@@@ (1)bucket_array size->" + lbucket_array.size() + "  combi_min->" + combi_min + "   combi_max->" + combi_max);

        jArrCombination_list = combination_bucket_select_array(value, param);
        Log.d(TAG, "#@# (2)@@@@@@@@@@ (2)jArrCombination_list ->" + jArrCombination_list.toString());

        jArrCombination.put(combination_bucket_sort_array(jArrCombination_list));
        //Log.d(TAG, "#@# @@@@@@@@@@ jArrCombination->" + jArrCombination.toString());
        return jArrCombination;
    }


    public JSONObject combin(int[] in_data, int combi_min, int boxweight_min, int boxweight_max) {

        int n = in_data.length;
        int[] n_arr = new int[n];
        for (int i = 0; i < n; i++) {
            n_arr[i] = i;
        }
        int sum = 0;

        Combination comb = new Combination(n_arr.length, combi_min);
        comb.combination(n_arr, 0, 0, 0);
        ArrayList<ArrayList<Integer>> result = comb.getResult();
        List<JSONObject> jsonValues = new ArrayList<JSONObject>();

        //Log.d(TAG,"#@# @@@@@@@@@@@@@@@@@모든조합 경우의수 result"+ result.toString());
        for (int i = 0; i < result.size(); i++) {
            sum = 0;
            JSONObject object = new JSONObject();
            JSONArray idx = new JSONArray();
            JSONArray value = new JSONArray();
            //Log.d(TAG,"#@# @@@@@@@@@@@@@@@@@모든조합 경우의수 result"+ result.toString());
            try {
                boolean ok = true;
                for (int j = 0; j < result.get(i).size(); j++) {
                    int i_idx = result.get(i).get(j);
                    int item_value = in_data[i_idx];
                    if (item_value <= 0) {
                        ok = false;//묷음중 값이 한개라도 제로인거 제외
                        break;
                    }
                    sum = sum + item_value;
                    value.put(item_value);
                    idx.put(i_idx);
                }
                int shift = (sum - boxweight_min);//마이너스오차는 제외시킴
                if (ok && (sum >= boxweight_min || sum <= boxweight_max) && (shift >= 0)) {
                    //int shift = (sum > boxweight_min) ? sum - boxweight_min : 10000;//마이너스오차는 제외시킴
                    object.put("weight", sum);
                    object.put("shift", shift);
                    object.put("idx", idx);
                    object.put("value", value);
                    jsonValues.add(object);
                    //Log.d(TAG,"#@# @@@@@@@(0) List jsonValues i["+i+"] ->"+object.toString());//jsonValues->List로 저장
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        //Log.d(TAG,"#@# @@@@@@@(1) List jsonValues["+ jsonValues.size()+"] ->"+jsonValues.toString());//jsonValues->List로 저장
        JSONArray sortedJsonArray = new JSONArray();
        Collections.sort(jsonValues, new Comparator<JSONObject>() {
            private static final String KEY_NAME = "shift";

            @Override
            public int compare(JSONObject a, JSONObject b) {
                int valA = 0;// = new String();
                int valB = 0;//String valB = new String();
                try {
                    valA = a.getInt(KEY_NAME);
                    valB = b.getInt(KEY_NAME);
                } catch (JSONException e) {
                }
                int val = (valA - valB) * (1);
                return val;
            }
        });
        for (int i = 0; i < jsonValues.size(); i++) {
            sortedJsonArray.put(jsonValues.get(i));
        }
//        for (int i = 0; i < sortedJsonArray.length(); i++) {
//              Log.d(TAG,"#@# @@@@@@@@@@ "+"cnt:"+sortedJsonArray.length()+"  sortedJsonArray->"+ sortedJsonArray.toString());
//        }
        //제일 첫번째만 남김++
        JSONObject jsonResult;
        jsonResult = new JSONObject();
        try {
            if (sortedJsonArray.length() > 0) jsonResult = sortedJsonArray.getJSONObject(0);
        } catch (JSONException e) {
            ;
        }
        //제일 첫번째만 남김--
        try {
            Thread.sleep(1);
            //TimeUnit.MICROSECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return jsonResult;
    }


}
