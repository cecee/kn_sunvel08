package com.cecee.ky_sunvel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.cecee.ky_sunvel.mp3.Mp3player;
import com.cecee.ky_sunvel.mp3.OnPlayerEventListener;
import com.cecee.ky_sunvel.mp3.SongDetails;
import com.google.android.material.slider.RangeSlider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;


public class MainActivity extends AppCompatActivity implements OnPlayerEventListener {
    private static final String TAG = "#@#";
    public static final int SCALE_PLATE_NUM = 10;
    public static final String MODEL_NAME = "채우는 선별기";

    //for mp3 player
    //private InputManager mInputManager;
    public Mp3player mp3_player = null;
    public ArrayList<SongDetails> songs = null;
    public LinearLayout linearLayout_b1_title, linearLayout_mp3_player;
    public TextView textView_mp3_title;
    public Button button_play_next, button_play_prev, button_play_stop;
    public String current_play_tile = "";

    public LinearLayout linearlayout_header, linearLayout_b1, linearLayout0, linearLayout1, linearLayout2;

    private ImageView imageView_character, imageView_date_from, imageView_date_to, imageView_test;
    public Button button_zero, button_seljung, seljung_return, button_naeyek, button_naeyek_return, button_history,button_box_zero,button_header_setup,button_box_clear;
    private Button ButtonView_combin_clear, ButtonView_system_clear, button_z01_0_adjust, button_z01_data;
    public Button button_item_title;
    public ImageButton button_add_item, button_remove_item;
    public RecyclerView recycleView_scale, recycleView_history;

    public ImageButton button_box_weight_low_dn,button_box_weight_low_up,button_box_weight_high_dn,button_box_weight_high_up;
    public ImageButton button_box_cnt_low_dn,button_box_cnt_low_up,button_box_cnt_high_dn,button_box_cnt_high_up;
    public ImageButton button_each_weight_low_dn,button_each_weight_low_up,button_each_weight_high_dn,button_each_weight_high_up;


    public TextView textView_b1_title, textView_b1_box_weight, textView_b1_box_weight_range, textView_x00_sum, textview_calendar_idx1, textView_z00_combi_cnt, textView_z00_combi_weight, textView_z00_weight_box;
    public TextView textView_date_from, textView_date_to, textView_chong_mugae, textViewChongBox, textViewBoxAvrMugae, textView_test_mode, textView_test_zero;
    public TextView textView_combi_sum1, textView_combi_sum2, textView_combi_sum3, textView_combi_sum4;
    public TextView textView_system_box_weight, textView_system_total_weight,textView_system_box_average_weight;
    public TextView textView_z00_date, textView_z02_date, textView_selected_title, textView_comb_num, textView_combi_weight, textView_box_weight;

    Timer timer = new Timer();

    public Current_Status currentStatus = new Current_Status(this);
    public ScaleRef scaleRef = new ScaleRef(this);
    public DB_Manager db_manager;// = new DB_Manager(this);
    public JniCall jni = new JniCall();
    public RangeSlider sliderView_combin_num, sliderView_combi_weight, sliderView_box_weight;
    public int menu_position = 0;
    public boolean FACTORY_TEST = false;
    public boolean RE_COMBINATION = false;
    public int loop_cnt = 0;
    public int title_click_cnt = 0;
    //EditText text;
    Spinner spinner;
    RecyclerManager recyclerManager;
    ClickAdapter clickAdapter;
    PeriodChangeCheckRunnable periodChangeCheckRunnable;
    DecimalFormat Formatter_3digit = new DecimalFormat("###,###");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        periodChangeCheckRunnable=new PeriodChangeCheckRunnable(this);
        clickAdapter = new ClickAdapter(this);
        db_manager = new DB_Manager(this);
        recyclerManager = new RecyclerManager(this);
        jni.jniOpenSerial();
        db_manager.db_create_table();
        db_manager.fun_setScaleReferenceFromDB();
        currentStatus.setProduct_jArry(db_manager.fun_GetInitProductDataFromDB("", "", true));
        currentStatus.mk_gCobination_Array_List(9,9);//7C7->nCr 9C9
        ArrayList<ArrayList<Integer>> aa= currentStatus.getgCobination_Array_List();
        Log.d(TAG,"#@# mk_gCobination_Array_List aa->->"+aa.toString());//test

        Init();

        sliderView_combin_num.addOnChangeListener(
                (sliderView_combin_num, value, fromUser) -> {
                    List<Float> _value = sliderView_combin_num.getValues();
                    Log.d("#@#","  addOnChangeListener->"+ _value.toString());
                    System.out.printf("#@# ##########sliderView_combin_num[%.1f] [%.1f]\n", _value.get(0), _value.get(1));
                    String str_combiNum = String.format("%d - %d 개", Math.round(_value.get(0)), Math.round(_value.get(1)));
                    textView_comb_num.setText(str_combiNum);
                }
        );
        sliderView_combi_weight.addOnChangeListener(
                (sliderView_combin_num, value, fromUser) -> {
                    List<Float> _value = sliderView_combin_num.getValues();
                    String str_combiWeight = String.format("%d - %d g", Math.round(_value.get(0)), Math.round(_value.get(1)));
                    textView_combi_weight.setText(str_combiWeight);
                }
        );
        sliderView_box_weight.addOnChangeListener(
                (sliderView_combin_num, value, fromUser) -> {
                    List<Float> _value = sliderView_combin_num.getValues();
                    System.out.printf("##########sliderView_combin_num[%.1f] [%.1f]\n", _value.get(0), _value.get(1));
                    String str_boxWeight = String.format("%.1f - %.1f Kg", _value.get(0), _value.get(1));
                    textView_box_weight.setText(str_boxWeight);
                }
        );

        imageView_character.setOnClickListener(clickAdapter);
        //button_zero.setOnTouchListener(clickAdapter);

        button_zero.setOnClickListener(clickAdapter);//0점조정
        button_box_zero.setOnClickListener(clickAdapter);//0점조정
        button_box_clear.setOnClickListener(clickAdapter);
        //button_zero.setOnTouchListener(clickAdapter);//TEST Flag
       // button_zero.setOnLongClickListener(clickAdapter);

        button_seljung.setOnClickListener(clickAdapter);//내역메뉴
        seljung_return.setOnClickListener(clickAdapter);

        button_naeyek.setOnClickListener(clickAdapter);//내역메뉴
        button_naeyek_return.setOnClickListener(clickAdapter);//내역에서 return

      //  button_header_setup.setOnClickListener(clickAdapter);
//        button_z01_0_adjust.setOnClickListener(clickAdapter);//0점조정
        ButtonView_system_clear.setOnClickListener(clickAdapter);//system value 초기화
        ButtonView_combin_clear.setOnClickListener(clickAdapter);//system value 초기화

//        textView_test_zero.setOnClickListener(clickAdapter);//test mode 0점 조정
//        textView_test_mode.setOnClickListener(clickAdapter);//저울교정 5kg 셋팅

        //button_z01_factor.setOnTouchListener(clickAdapter);//저울교정 5kg 셋팅
//        imageView_test.setOnTouchListener(clickAdapter);//TEST Flag



        imageView_date_from.setOnClickListener(clickAdapter);
        imageView_date_to.setOnClickListener(clickAdapter);
        button_history.setOnClickListener(clickAdapter);
        button_add_item.setOnClickListener(clickAdapter);
        button_remove_item.setOnClickListener(clickAdapter);

        button_box_weight_low_up.setOnClickListener(clickAdapter);
        button_box_weight_low_dn.setOnClickListener(clickAdapter);
        button_box_weight_high_dn.setOnClickListener(clickAdapter);
        button_box_weight_high_up.setOnClickListener(clickAdapter);

        button_box_cnt_low_dn.setOnClickListener(clickAdapter);
        button_box_cnt_low_up.setOnClickListener(clickAdapter);
        button_box_cnt_high_dn.setOnClickListener(clickAdapter);
        button_box_cnt_high_up.setOnClickListener(clickAdapter);

        button_each_weight_low_dn.setOnClickListener(clickAdapter);
        button_each_weight_low_up.setOnClickListener(clickAdapter);
        button_each_weight_high_dn.setOnClickListener(clickAdapter);
        button_each_weight_high_up.setOnClickListener(clickAdapter);

        textView_z02_date.setOnClickListener(clickAdapter);//날짜 시간 세팅
       // textView_z00_date.setOnClickListener(clickAdapter);//날짜세팅
        spinner = (Spinner) findViewById(R.id.spinner);

        //for mp3player - start
        IntentFilter media_filter = new IntentFilter();
        media_filter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
        media_filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        media_filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        media_filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        media_filter.addAction(Intent.ACTION_MEDIA_EJECT);
        media_filter.addAction(Intent.ACTION_MEDIA_NOFS);
        media_filter.addDataScheme("file");
        registerReceiver(mBRMounted, media_filter);

        button_play_prev.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp3_player != null) {
                    mp3_player.previousSong();
                    button_play_stop.setText("정지");
                    textView_mp3_title.setSelected(true);
                }
            }
        });
        button_play_next.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp3_player != null) {
                    mp3_player.nextSong();
                    button_play_stop.setText("정지");
                    textView_mp3_title.setSelected(true);
                }
            }
        });

        button_play_stop.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp3_player != null) {
                    if(mp3_player.isPlaying()) {
                        mp3_player.pause();
                        textView_mp3_title.setText(MODEL_NAME);
                        button_play_stop.setText("재생");
                        textView_mp3_title.setSelected(false);
                    } else {
                        mp3_player.pause();
                        button_play_stop.setText("정지");
                        textView_mp3_title.setText(current_play_tile);
                        textView_mp3_title.setSelected(true);
                    }
                }
            }
        });
        startMp3();
        //for mp3 player - end

        db_manager.spinnerViewFromDB();
        db_manager.funTitleViewInitFromDB();

        ///current date time set++
        currentStatus.fun_CurrentDateTimeFromSystemTime();
        textView_z00_date.setText(currentStatus.current_date_time);
        //textView_z02_date.setText(currentStatus.current_date_time);
        ///current date time set--

        Thread pcc = new Thread(periodChangeCheckRunnable);
        pcc.start();
        //timer.schedule(timerTask, 0, 700); //Timer 실행
    }

    void spinnerViewFromObject(JSONArray jArr) {
        int item_cnt=jArr.length();
        if(item_cnt==0){
            item_cnt=1;
        }
        String[] str_names = new String[item_cnt];
        str_names[0]="";
        for (int i = 0; i < item_cnt; i++) {
            try {
                JSONObject jobj = jArr.getJSONObject(i);
                str_names[i] = jobj.getString("sname");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        textView_selected_title.setText(str_names[0]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner_custom_select, R.id.content_text, str_names);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String str = adapterView.getItemAtPosition(i).toString();
                System.out.printf("#@# ##########onItemSelected[%d][%s]\n", i, str);
                db_manager.dbSelectedSpinner(str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // System.out.printf("#@# ##########onNothingSelected----\n");
            }
        });

    }

    void Init() {
        linearlayout_header = (LinearLayout) findViewById(R.id.linearlayout_header);
        linearLayout_b1 = (LinearLayout) findViewById(R.id.linearlayout_b1);
        linearLayout0 = (LinearLayout) findViewById(R.id.linearlayout0);
        linearLayout1 = (LinearLayout) findViewById(R.id.linearlayout1);
        linearLayout2 = (LinearLayout) findViewById(R.id.linearlayout2);

        linearLayout_b1.setVisibility(View.VISIBLE);
        linearLayout0.setVisibility(View.INVISIBLE);
        linearLayout1.setVisibility(View.INVISIBLE);
        linearLayout2.setVisibility(View.INVISIBLE);

        imageView_date_from = (ImageView) findViewById(R.id.imageView_date_from);
        imageView_date_to = (ImageView) findViewById(R.id.imageView_date_to);
        imageView_test = (ImageView) findViewById(R.id.imageView_test);
        imageView_character = (ImageView) findViewById(R.id.imageView_character);

        button_zero= (Button) findViewById(R.id.button_zero);
        button_box_zero= (Button) findViewById(R.id.button_box_zero);
        button_box_clear= (Button) findViewById(R.id.button_box_clear);

        seljung_return= (Button) findViewById(R.id.seljung_return);
        button_seljung = (Button) findViewById(R.id.button_seljung);
        button_naeyek = (Button) findViewById(R.id.button_naeyek);
        button_naeyek_return = (Button) findViewById(R.id.button_naeyek_return);

        button_header_setup = (Button) findViewById(R.id.button_header_setup);
        //button_z00_menu = (Button) findViewById(R.id.button_z00_menu);
        button_item_title = (Button) findViewById(R.id.button_item_title);
//        button_z01_0_adjust = (Button) findViewById(R.id.button_z01_0_adjust);
        //button_z01_factor = (Button) findViewById(R.id.button_z01_factor);

  //      button_z01_data = (Button) findViewById(R.id.button_z01_data);
        textView_b1_title= (TextView) findViewById(R.id.textView_b1_title);
        textView_b1_title.setText(MODEL_NAME);
        textView_b1_box_weight= (TextView) findViewById(R.id.textView_b1_box_weight);
        textView_b1_box_weight_range= (TextView) findViewById(R.id.textView_b1_box_weight_range);

        textView_z00_date = (TextView) findViewById(R.id.textView_z00_date);
        textView_z02_date = (TextView) findViewById(R.id.textView_z02_date);

        textView_z00_combi_cnt = (TextView) findViewById(R.id.textView_z00_combi_cnt);
        textView_z00_combi_weight = (TextView) findViewById(R.id.textView_z00_combi_weight);
        textView_z00_weight_box = (TextView) findViewById(R.id.textView_z00_weight_box);

        recycleView_scale = (RecyclerView) findViewById(R.id.recycleView_scale);
        recycleView_history = (RecyclerView) findViewById(R.id.recycleView_history);
        button_add_item = (ImageButton) findViewById(R.id.button_add_item);

        button_box_weight_low_dn= (ImageButton) findViewById(R.id.button_box_weight_low_dn);
        button_box_weight_low_up= (ImageButton) findViewById(R.id.button_box_weight_low_up);
        button_box_weight_high_dn= (ImageButton) findViewById(R.id.button_box_weight_high_dn);
        button_box_weight_high_up= (ImageButton) findViewById(R.id.button_box_weight_high_up);

        button_box_cnt_low_dn= (ImageButton) findViewById(R.id.button_box_cnt_low_dn);
        button_box_cnt_low_up= (ImageButton) findViewById(R.id.button_box_cnt_low_up);
        button_box_cnt_high_dn= (ImageButton) findViewById(R.id.button_box_cnt_high_dn);
        button_box_cnt_high_up= (ImageButton) findViewById(R.id.button_box_cnt_high_up);

        button_each_weight_low_dn= (ImageButton) findViewById(R.id.button_each_weight_low_dn);
        button_each_weight_low_up= (ImageButton) findViewById(R.id.button_each_weight_low_up);
        button_each_weight_high_dn= (ImageButton) findViewById(R.id.button_each_weight_high_dn);
        button_each_weight_high_up= (ImageButton) findViewById(R.id.button_each_weight_high_up);


        button_remove_item = (ImageButton) findViewById(R.id.button_remove_item);



        sliderView_combin_num = (RangeSlider) findViewById(R.id.sliderView_combin_num);
        sliderView_combi_weight = (RangeSlider) findViewById(R.id.sliderView_combi_weight);
        sliderView_box_weight = (RangeSlider) findViewById(R.id.sliderView_box_weight);

        textView_selected_title = (TextView) findViewById(R.id.textView_selected_title);
        textView_comb_num = (TextView) findViewById(R.id.textView_comb_num);
        textView_combi_weight = (TextView) findViewById(R.id.textView_combi_weight);
        textView_box_weight = (TextView) findViewById(R.id.textView_box_weight);

        button_history = (Button) findViewById(R.id.button_history);
        ButtonView_system_clear = (Button) findViewById(R.id.ButtonView_system_clear);
        ButtonView_combin_clear = (Button) findViewById(R.id.ButtonView_combin_clear);

        textView_date_from = (TextView) findViewById(R.id.textView_date_from);
        textView_date_to = (TextView) findViewById(R.id.textView_date_to);
        textView_chong_mugae = (TextView) findViewById(R.id.textView_chong_mugae);
        textViewChongBox = (TextView) findViewById(R.id.textViewChongBox);
        textViewBoxAvrMugae = (TextView) findViewById(R.id.textViewBoxAvrMugae);
        textView_combi_sum1 = (TextView) findViewById(R.id.textView_combi_sum1);
        textView_combi_sum2 = (TextView) findViewById(R.id.textView_combi_sum2);
        textView_combi_sum3 = (TextView) findViewById(R.id.textView_combi_sum3);
        textView_combi_sum4 = (TextView) findViewById(R.id.textView_combi_sum4);
        //textView_system_box_num = (TextView) findViewById(R.id.textView_system_box_weight);
        textView_system_box_weight = (TextView) findViewById(R.id.textView_system_box_weight);
        textView_system_total_weight = (TextView) findViewById(R.id.textView_system_total_weight);
        textView_system_box_average_weight = (TextView) findViewById(R.id.textView_system_box_average_weight);
        textView_test_mode = (TextView) findViewById(R.id.textView_test_mode);
        textView_test_zero = (TextView) findViewById(R.id.textView_test_zero);

        textView_date_from.setText("2021-06-01");
        textView_date_to.setText(currentStatus.getCurrent_date());

        //for mp3 player - start
        textView_mp3_title = (TextView) findViewById(R.id.textView_mp3_title);
        linearLayout_b1_title = (LinearLayout) findViewById(R.id.linearLayout_b1_title);
        linearLayout_mp3_player = (LinearLayout) findViewById(R.id.linearLayout_mp3_player);

        button_play_prev = (Button) findViewById(R.id.button_play_prev);
        button_play_next = (Button) findViewById(R.id.button_play_next);
        button_play_stop = (Button) findViewById(R.id.button_play_stop);
        //for mp3 player - end

        recyclerManager.recycle_scale_init();
        recyclerManager.recycler_history_init();
    }

    //for mp3 player - start
    void startMp3() {
        Log.d(TAG,"MP3_startMp3");
        getAllSongs();

        if (mp3_player != null) {
            mp3_player.stop();
            mp3_player = null;
        }

        mp3_player = new Mp3player(this);
        mp3_player.init(songs);
        mp3_player.play();
        textView_mp3_title.setSelected(true);

        linearLayout_b1_title.setVisibility(View.INVISIBLE);
        linearLayout_mp3_player.setVisibility(View.VISIBLE);
    }

    void stopMp3() {
        Log.d(TAG,"MP3_stopMp3");
        linearLayout_b1_title.setVisibility(View.VISIBLE);
        linearLayout_mp3_player.setVisibility(View.INVISIBLE);
        if (mp3_player != null) {
            mp3_player.stop();
            mp3_player = null;
        }
    }

    @Override
    public void onPlayerSongComplete() {
        mp3_player.nextSong();
        Log.d(TAG,"MP3_onPlayerSongComplete");
    }

    @Override
    public void onPlayerSongStart(String Title, int songDuration, int songPosition) {
        current_play_tile = Title;
        textView_mp3_title.setText(Title);
        Log.d(TAG,"MP3_onPlayerSongStart "+ Title);
    }

    private void getAllSongs()
    {
        //creating selection for the database
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        final String[] projection = new String[] {
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA};

        //creating sort by for database
        final String sortOrder = MediaStore.Audio.AudioColumns.TITLE
                + " COLLATE LOCALIZED ASC";

        //stating pointer
        Cursor cursor = null;

        try {
            //the table for query
            Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            // query the db
            cursor = getBaseContext().getContentResolver().query(uri,
                    projection, selection, null, sortOrder);
            if (cursor != null) {
                songs = new ArrayList<SongDetails>(cursor.getCount());
                cursor.moveToFirst();

                SongDetails details;
                while (!cursor.isAfterLast()) {
                    details = new SongDetails();
                    details.songTitle = cursor.getString(0);
                    details.songArtist = cursor.getString(1);
                    details.songData = cursor.getString(2);
                    songs.add(details);
                    cursor.moveToNext();
                    Log.d(TAG,"MP3_"+ details.songTitle + " / " + details.songArtist + " / " + details.songData);
                }
            }
        } catch (Exception ex) {

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    BroadcastReceiver mBRMounted = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String path = intent.getData().getPath();

            switch(action) {
                case Intent.ACTION_MEDIA_SCANNER_FINISHED:
                    startMp3(); //미디어 스캔 완료후 시작해야 음악파일찾을수 있음
                    Log.d(TAG, "MP3_[BroadcastReceiver] ACTION_MEDIA_SCANNER_FINISHED" + path);
                    break;
                case Intent.ACTION_MEDIA_MOUNTED:
                    Log.d(TAG, "MP3_[BroadcastReceiver] ACTION_MEDIA_MOUNTED" + path);
                    break;
                case Intent.ACTION_MEDIA_UNMOUNTED:
                    Log.d(TAG, "MP3_[BroadcastReceiver] ACTION_MEDIA_UNMOUNTED" + path);
                    stopMp3();
                    break;
                case Intent.ACTION_MEDIA_REMOVED:
                    Log.d(TAG, "MP3_[BroadcastReceiver] ACTION_MEDIA_REMOVED" + path);
                    stopMp3();
                    break;
                case Intent.ACTION_MEDIA_EJECT:
                    Log.d(TAG, "MP3_[BroadcastReceiver] ACTION_MEDIA_EJECT" + path);
                    stopMp3();
                    break;
                case Intent.ACTION_MEDIA_NOFS:
                    stopMp3();
                    Log.d(TAG, "MP3_[BroadcastReceiver] ACTION_MEDIA_NOFS" + path);
                    break;
            }
        }
    };
    //for mp3 player - end
}