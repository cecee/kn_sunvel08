package com.cecee.ky_sunvel;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.LTGRAY;
import static android.graphics.Color.WHITE;


public class RecyclerAdapterScale extends RecyclerView.Adapter<RecyclerAdapterScale.ScaleViewHolder> {
    private static final String TAG = "#@#";
    private Context context;
    private Current_Status currentStatus;
    private int[] scale;
    private  int[] param;
    private int[] bg_color;
    private String str;

    public static class ScaleViewHolder extends RecyclerView.ViewHolder {
        //recycleView안에 단위블럭을 v로 받고 v에 구성되어 있는 각각의 view를 인스턴스화한다.
        //지금은 텍스트뷰 하나밖에 없다.
        private final LinearLayout recycle_scale;
        private final TextView textViewScaleValue;
        private final Button buttonScalePos;

        public ScaleViewHolder(View v) {
            super(v);
            recycle_scale = v.findViewById(R.id.recycle_scale);
            textViewScaleValue = v.findViewById(R.id.textViewScaleValue);
            buttonScalePos = v.findViewById(R.id.buttonScalePos);
            // relativelayout_recycle_scale=v.findViewById(R.id.relativelayout_recycle_scale);
        }
    }

    public RecyclerAdapterScale(Current_Status dataSet) {
        //currentStatus = dataSet;
       // this.bg_color = dataSet.getItemViewColor();
        this.scale = dataSet.getScale_data();
        //Log.d("#@#","@@@@@@@@@@@ScaleAdapter scale:->"+scale[2]);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ScaleViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        //똥가리 전체를 감싼뷰
        //scale = scaleData.getArryValue();
        //Log.d("#@#","scale:->"+scale.toString());
        LinearLayout v = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycle_scale_item, viewGroup, false);//xml file 이름
        ScaleViewHolder vh = new ScaleViewHolder(v);
        context = viewGroup.getContext();
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(ScaleViewHolder viewHolder, final int position) {
        //int index[] = {0, 7, 1, 8, 2, 9, 3, 10, 4, 11, 5, 12, 6, 13};
        int index[] = {0, 1, 2, 3, 4, 5, 6,7, 8};
        int pos = index[position];
        int value_pos = position+1;
         bg_color = ((MainActivity) context).currentStatus.getItemViewColor();
         param= ((MainActivity) context).currentStatus.getCurrent_parameter();
        //Log.d(TAG,"#@# @@@@@@@@@@(x) onBindViewHolder ->"+ Arrays.toString(param));
        int item_Weight_min=param[2];
        int item_Weight_max=param[3];

       //str = String.valueOf(scale[0]);
       //every time display++
       // int sum=scale[0];
        str =String.format("%.2f",(float)scale[0]/1000);
        ((MainActivity) context).textView_b1_box_weight.setText(str);
        //every time display--

//        if(bg_color[0]==1){
//            ((MainActivity) context).textView_b1_box_weight.setTextColor(Color.RED);
//        }
//        else ((MainActivity) context).textView_b1_box_weight.setTextColor(BLACK);

        LinearLayout.LayoutParams params_pos = (LinearLayout.LayoutParams) viewHolder.buttonScalePos.getLayoutParams();
        int _color;
        if(((MainActivity) context).FACTORY_TEST) bg_color[pos]=0;

        switch (bg_color[pos+2]) {
            case 1: //8개짜리 좀어두운 LED
            case 15: //12개짜리 밝은 LED
                _color = Color.GREEN;
                //_color = ContextCompat.getColor(((MainActivity) context), R.color.combi_1);
                break;
            case 2:
                _color = ContextCompat.getColor(((MainActivity) context), R.color.combi_2);
                break;
            case 3:
                _color = ContextCompat.getColor(((MainActivity) context), R.color.combi_3);
                break;
            case 4:
                _color = ContextCompat.getColor(((MainActivity) context), R.color.combi_4);
                break;
            default:
               // _color = ContextCompat.getColor(((MainActivity) context), R.color.white);
                _color = BLACK;
                break;
        }
        if(scale[value_pos]<item_Weight_min && scale[value_pos]>5 && !((MainActivity) context).FACTORY_TEST) {
            //_color = ContextCompat.getColor(((MainActivity) context), R.color.combi_weight_min);

            //_color = ((MainActivity) context), R.color.weight_low);
            _color =ContextCompat.getColor(((MainActivity) context), R.color.weight_low);
            str = ((MainActivity) context).getString(R.string.emogi_irregular);
            str =String.format("%s %d",str,scale[value_pos]);
           // str ="미달";
        }
        else if(scale[value_pos]>item_Weight_max && !((MainActivity) context).FACTORY_TEST) {
            //_color = ContextCompat.getColor(((MainActivity) context), R.color.combi_weight_max);
            _color =ContextCompat.getColor(((MainActivity) context), R.color.weight_high);
            str = ((MainActivity) context).getString(R.string.emogi_irregular);
            str =String.format("%s %d",str,scale[value_pos]);
            //str ="초과";
        }
        else{
            str = String.valueOf(scale[value_pos]);
        }

        //str = String.valueOf(scale[pos]);
        viewHolder.textViewScaleValue.setText(str);
        //str = String.valueOf(position);
        viewHolder.buttonScalePos.setText(String.valueOf(position+1));
     //   viewHolder.textViewScaleValue.setBackground(ContextCompat.getDrawable(context, R.drawable.recycler_item_bg));
     //   viewHolder.textViewScaleValue.getBackground().setColorFilter(_color, PorterDuff.Mode.SRC_ATOP);

        GradientDrawable circle_gd = new GradientDrawable();
        circle_gd.setColor(_color);
        circle_gd.setCornerRadius(45);

        if (bg_color[pos] == 0) {
            viewHolder.textViewScaleValue.setTextColor(BLACK);
            viewHolder.buttonScalePos.setTextColor(WHITE);
           // circle_gd.setStroke(4, LTGRAY);
        } else {
            viewHolder.textViewScaleValue.setTextColor(BLACK);
            viewHolder.buttonScalePos.setTextColor(WHITE);
          //  circle_gd.setStroke(4, WHITE);
        }

        viewHolder.buttonScalePos.setBackgroundDrawable(circle_gd);
//        if (position % 2 == 0) {
//            params_pos.setMargins(390, 0, 0, 0);
//            viewHolder.buttonScalePos.setLayoutParams(params_pos);
//        } else {
//            params_pos.setMargins(10, 0, 0, 0);
//            viewHolder.buttonScalePos.setLayoutParams(params_pos);
//        }
        params_pos.setMargins(5, 5, 5, 0);
        viewHolder.buttonScalePos.setLayoutParams(params_pos);
    }

    @Override
    public int getItemCount() {
        return 8;
        // return (scale==null) ? 0 : scale.length;
    }


}